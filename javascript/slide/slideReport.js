$(function() {
    var count = 0;
    var Timer = setInterval(passa, 4000);

    function passa() {
        count++;
        Slide();
    }

    function Slide() {
        if (count > 3) { count = 0 }
        $('.dot').removeClass('on');
        $('.dot[v=' + '"' + count + '"' + ']').addClass('on');
        $('#slide').css('left', "-" + count * 100 + "%");
    }

    $('.dot').click(function() {
        clearInterval(Timer);
        count = Number($(this).attr('v'));
        Timer = setInterval(passa, 4000);
        Slide();
    });
});